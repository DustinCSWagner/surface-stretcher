#!/usr/bin/env ruby
# frozen_string_literal: true

require 'mini_magick'


################# Opposite walls           Corridor Walls                          Number of Images
# 0 1 2 3 4 5 6 #   <-- These blocks are calculated for generating AB..FG
# A|B|C|D|E|F|G #                          [AB, BC, CD, DE, EF, FG]                       6
# ¯ ¯ ¯ ¯ ¯ ¯ ¯ # [A, B, C, D, E, F, G]                                                   7
#  |H|I|J|K|L|  #                          [H_AB, HI_BC, IJ_CD, JK_DE, KL_EF, L_FG]       6
#   ¯ ¯ ¯ ¯ ¯   # [H, I, J, K, L]                                                         5
#    |M|N|O|    #                          [M_HI, MN_IJ, NO_JK, O_KL]                     4
#     ¯ ¯ ¯     # [M, N, O]                                                               3
#     P|@|Q     #                          [P, Q]                                         2
#################                                                                      -------
#                                                                                        33

# Visible Map example
# 0: ###     :d3 ABCDEFG
# 1: ### ### :d2  HIJKL
# 2: ####### :d1   MNO
# 3: ## a### :d0   P Q
# col0123456
# [row] [col]

## Parameters
DEBUG = false
# starting input image
#input = './input/grass-brick-1280.png'
input = ARGV[0]

# darkness settings
dist1_percentage = '0%'
dist2_percentage = '15%'
dist3_percentage = '35%'
dist4_percentage = '65%'
dist5_percentage = '85%'
dist6_percentage = '90%'
dist7_percentage = '95%'

# starting source texture resolution in pixels, this will have perspective distortions applied to give the illusion of depth
w = 1280

# this is the viewport dimension used to display pre-rendered walls in-game. The transformed images are centered and cropped to fit.
view_x = 1080
view_y = 720

# these are the pixel widths for the central opposite walls.
# d is furthest
d_width = 160
# j is mid
j_width = d_width * 2
# n is closest
n_width = d_width * 2 * 2

d_half_width = (d_width / 2).to_i

first_row_bottom = (w - n_width) / 2
first_row_top = w - first_row_bottom

second_row_bottom = (w - j_width) / 2
second_row_top = w - second_row_bottom

third_row_bottom = (w - d_width) / 2
third_row_top =  w - third_row_bottom

fourth_row_bottom = (w - d_half_width) / 2
fourth_row_top =  w - fourth_row_bottom

## First row verticals
n_left_vert = first_row_bottom
n_right_vert = first_row_top
# these are off the source w dimensions
m_left_vert = n_left_vert - n_width
o_right_vert = n_right_vert + n_width

## Second row verticals
j_left_vert = second_row_bottom
j_right_vert = second_row_top
i_left_vert = j_left_vert - j_width
k_right_vert = j_right_vert + j_width
# these are off the source w dimensions
h_left_vert = i_left_vert - j_width
l_right_vert = k_right_vert + j_width

## Third row verticals
d_left_vert = third_row_bottom
d_right_vert = third_row_top
c_left_vert = d_left_vert - d_width
b_left_vert = c_left_vert - d_width
a_left_vert = b_left_vert - d_width
e_right_vert = d_right_vert + d_width
f_right_vert = e_right_vert + d_width
g_right_vert = f_right_vert + d_width

## Forth row verticals
three_left_vert = fourth_row_bottom
three_right_vert = fourth_row_top
two_left_vert = three_left_vert - d_half_width
one_left_vert = two_left_vert - d_half_width
zero_left_vert = one_left_vert - d_half_width
four_right_vert = three_right_vert + d_half_width
five_right_vert = four_right_vert + d_half_width
six_right_vert = five_right_vert + d_half_width

## Functions and Class
# Class structure to hold the x1, y1 to x2, y2 perspective transformation coordinates
class Transform
  def initialize(x1, y1, x2, y2)
    @x1 = x1
    @y1 = y1
    @x2 = x2
    @y2 = y2
  end

  def to_s
    "(#{@x1},#{@y1}) -> (#{@x2},#{@y2})"
  end
  attr_accessor :x1, :y1, :x2, :y2
end

# transform function that takes a source image and writes the output imgage
def dungeon_transfrom(_source_img, _w, coords, _darkness, _vx, _vy, _output_img)
  tl = coords[0] # top left
  bl = coords[1] # bottom left
  tr = coords[2] # top right
  br = coords[3] # bottom right
  if DEBUG
    puts '= dt ='
    puts tl.to_s
    puts bl.to_s
    puts tr.to_s
    puts br.to_s
  end
  MiniMagick::Tool::Magick.new do |convert|
    convert << _source_img
    convert.merge! ['-virtual-pixel', 'transparent']
    convert.fill('#0c131e').colorize(_darkness)
    convert.distort('Perspective', [[tl.x1, tl.y1, tl.x2, tl.y2].join(','),
                                    [bl.x1, bl.y1, bl.x2, bl.y2].join(','),
                                    [tr.x1, tr.y1, tr.x2, tr.y2].join(','),
                                    [br.x1, br.y1, br.x2, br.y2].join(',')].join(' '))
    # crop to viewport size
    convert.crop("#{_vx}x#{_vy}+#{((_w - _vx) / 2).to_i}+#{((_w - _vy) / 2).to_i}")
    # make sure other areas are transparent, this will delete the to the right of the image
    convert.merge! ['-region', "#{(_vx / 2).to_i}x#{_vy.to_i}+#{(_vx / 2).to_i}+0", '-fill',
                    'red', '-colorize', '30%']
    # output
    convert << _output_img
  end
end

# transform function that takes a source image and writes the output imgage
def dungeon_transfrom_center(_source_img, _w, coords, _darkness, _vx, _vy, _output_img)
  tl = coords[0] # top left
  bl = coords[1] # bottom left
  tr = coords[2] # top right
  br = coords[3] # bottom right
  if DEBUG
    puts '= dt ='
    puts tl.to_s
    puts bl.to_s
    puts tr.to_s
    puts br.to_s
  end
  MiniMagick::Tool::Magick.new do |convert|
    convert << _source_img
    convert.merge! ['-virtual-pixel', 'transparent']
    convert.fill('#0c131e').colorize(_darkness)
    convert.distort('Perspective', [[tl.x1, tl.y1, tl.x2, tl.y2].join(','),
                                    [bl.x1, bl.y1, bl.x2, bl.y2].join(','),
                                    [tr.x1, tr.y1, tr.x2, tr.y2].join(','),
                                    [br.x1, br.y1, br.x2, br.y2].join(',')].join(' '))
    # crop to viewport size
    convert.crop("#{_vx}x#{_vy}+#{((_w - _vx) / 2).to_i}+#{((_w - _vy) / 2).to_i}")
    # output
    convert << _output_img
  end
end

# transform function that takes a source image and writes a left-right flip image as output
def dungeon_transfrom_flip(_source_img, _output_img)
  image = MiniMagick::Image.open(_source_img)
  image.flop
  image.write(_output_img)
end

# Debuging calculations
if DEBUG
  puts '=== sizes ===='
  puts d_width
  puts j_width
  puts n_width
  puts '=== first row ===='
  puts first_row_bottom
  puts first_row_top
  puts '=== second row ===='
  puts second_row_bottom
  puts second_row_top
  puts '=== third row ===='
  puts third_row_bottom
  puts third_row_top
  puts '=== fourth row ===='
  puts fourth_row_bottom
  puts fourth_row_top
  puts '=== first row verticals===='
  puts m_left_vert
  puts n_left_vert
  puts n_right_vert
  puts o_right_vert
  puts '=== second row verticals===='
  puts h_left_vert
  puts i_left_vert
  puts j_left_vert
  puts j_right_vert
  puts k_right_vert
  puts l_right_vert
  puts '=== third row verticals===='
  puts a_left_vert
  puts b_left_vert
  puts c_left_vert
  puts d_left_vert
  puts d_right_vert
  puts e_right_vert
  puts f_right_vert
  puts g_right_vert
  puts '=== fourth row verticals===='
  puts zero_left_vert
  puts one_left_vert
  puts two_left_vert
  puts three_left_vert
  puts three_right_vert
  puts four_right_vert
  puts five_right_vert
  puts six_right_vert
end

## Convert source image
# [top_left, bottom_left, top_right, bottom_right]
# the left side images
dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, 0, w),
                   Transform.new(0, 0, 0, 0),
                   Transform.new(w, w, n_left_vert, first_row_top),
                   Transform.new(w, 0, n_left_vert, first_row_bottom)],
                  dist1_percentage,
                  view_x, view_y,
                  './output/p.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, m_left_vert, first_row_top),
                   Transform.new(0, 0, m_left_vert, first_row_bottom),
                   Transform.new(w, w, n_left_vert, first_row_top),
                   Transform.new(w, 0, n_left_vert, first_row_bottom)],
                  dist2_percentage,
                  view_x, view_y,
                  './output/m.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, n_left_vert, first_row_top),
                   Transform.new(0, 0, n_left_vert, first_row_bottom),
                   Transform.new(w, w, j_left_vert, second_row_top),
                   Transform.new(w, 0, j_left_vert, second_row_bottom)],
                  dist3_percentage,
                  view_x, view_y,
                  './output/mn_ij.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, m_left_vert, first_row_top),
                   Transform.new(0, 0, m_left_vert, first_row_bottom),
                   Transform.new(w, w, i_left_vert, second_row_top),
                   Transform.new(w, 0, i_left_vert, second_row_bottom)],
                  dist3_percentage,
                  view_x, view_y,
                  './output/m_hi.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, i_left_vert, second_row_top),
                   Transform.new(0, 0, i_left_vert, second_row_bottom),
                   Transform.new(w, w, j_left_vert, second_row_top),
                   Transform.new(w, 0, j_left_vert, second_row_bottom)],
                  dist4_percentage,
                  view_x, view_y,
                  './output/i.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, h_left_vert, second_row_top),
                   Transform.new(0, 0, h_left_vert, second_row_bottom),
                   Transform.new(w, w, i_left_vert, second_row_top),
                   Transform.new(w, 0, i_left_vert, second_row_bottom)],
                  dist4_percentage,
                  view_x, view_y,
                  './output/h.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, j_left_vert, second_row_top),
                   Transform.new(0, 0, j_left_vert, second_row_bottom),
                   Transform.new(w, w, d_left_vert, third_row_top),
                   Transform.new(w, 0, d_left_vert, third_row_bottom)],
                  dist5_percentage,
                  view_x, view_y,
                  './output/ij_cd.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, i_left_vert, second_row_top),
                   Transform.new(0, 0, i_left_vert, second_row_bottom),
                   Transform.new(w, w, c_left_vert, third_row_top),
                   Transform.new(w, 0, c_left_vert, third_row_bottom)],
                  dist5_percentage,
                  view_x, view_y,
                  './output/hi_bc.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, h_left_vert, second_row_top),
                   Transform.new(0, 0, h_left_vert, second_row_bottom),
                   Transform.new(w, w, b_left_vert, third_row_top),
                   Transform.new(w, 0, b_left_vert, third_row_bottom)],
                  dist5_percentage,
                  view_x, view_y,
                  './output/h_ab.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, c_left_vert, third_row_top),
                   Transform.new(0, 0, c_left_vert, third_row_bottom),
                   Transform.new(w, w, d_left_vert, third_row_top),
                   Transform.new(w, 0, d_left_vert, third_row_bottom)],
                  dist6_percentage,
                  view_x, view_y,
                  './output/c.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, b_left_vert, third_row_top),
                   Transform.new(0, 0, b_left_vert, third_row_bottom),
                   Transform.new(w, w, c_left_vert, third_row_top),
                   Transform.new(w, 0, c_left_vert, third_row_bottom)],
                  dist6_percentage,
                  view_x, view_y,
                  './output/b.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, a_left_vert, third_row_top),
                   Transform.new(0, 0, a_left_vert, third_row_bottom),
                   Transform.new(w, w, b_left_vert, third_row_top),
                   Transform.new(w, 0, b_left_vert, third_row_bottom)],
                  dist6_percentage,
                  view_x, view_y,
                  './output/a.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, b_left_vert, third_row_top),
                   Transform.new(0, 0, b_left_vert, third_row_bottom),
                   Transform.new(w, w, one_left_vert, fourth_row_top),
                   Transform.new(w, 0, one_left_vert, fourth_row_bottom)],
                  dist7_percentage,
                  view_x, view_y,
                  './output/ab.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, c_left_vert, third_row_top),
                   Transform.new(0, 0, c_left_vert, third_row_bottom),
                   Transform.new(w, w, two_left_vert, fourth_row_top),
                   Transform.new(w, 0, two_left_vert, fourth_row_bottom)],
                  dist7_percentage,
                  view_x, view_y,
                  './output/bc.png')

dungeon_transfrom(input,
                  w,
                  [Transform.new(0, w, d_left_vert, third_row_top),
                   Transform.new(0, 0, d_left_vert, third_row_bottom),
                   Transform.new(w, w, three_left_vert, fourth_row_top),
                   Transform.new(w, 0, three_left_vert, fourth_row_bottom)],
                  dist7_percentage,
                  view_x, view_y,
                  './output/cd.png')

# the centers
dungeon_transfrom_center(input,
                         w,
                         [Transform.new(0, w, n_left_vert, first_row_top),
                          Transform.new(0, 0, n_left_vert, first_row_bottom),
                          Transform.new(w, w, n_right_vert, first_row_top),
                          Transform.new(w, 0, n_right_vert, first_row_bottom)],
                         dist2_percentage,
                         view_x, view_y,
                         './output/n.png')

dungeon_transfrom_center(input,
                         w,
                         [Transform.new(0, w, j_left_vert, second_row_top),
                          Transform.new(0, 0, j_left_vert, second_row_bottom),
                          Transform.new(w, w, j_right_vert, second_row_top),
                          Transform.new(w, 0, j_right_vert, second_row_bottom)],
                         dist4_percentage,
                         view_x, view_y,
                         './output/j.png')

dungeon_transfrom_center(input,
                         w,
                         [Transform.new(0, w, d_left_vert, third_row_top),
                          Transform.new(0, 0, d_left_vert, third_row_bottom),
                          Transform.new(w, w, d_right_vert, third_row_top),
                          Transform.new(w, 0, d_right_vert, third_row_bottom)],
                         dist6_percentage,
                         view_x, view_y,
                         './output/d.png')

# mirror for the right side
# first row
dungeon_transfrom_flip('./output/p.png', './output/q.png')
dungeon_transfrom_flip('./output/m.png', './output/o.png')
dungeon_transfrom_flip('./output/mn_ij.png', './output/no_jk.png')
dungeon_transfrom_flip('./output/m_hi.png', './output/o_kl.png')

# second row
dungeon_transfrom_flip('./output/h.png', './output/l.png')
dungeon_transfrom_flip('./output/i.png', './output/k.png')
dungeon_transfrom_flip('./output/hi_bc.png', './output/kl_ef.png')
dungeon_transfrom_flip('./output/ij_cd.png', './output/jk_de.png')
dungeon_transfrom_flip('./output/h_ab.png', './output/l_fg.png')

# third row
dungeon_transfrom_flip('./output/a.png', './output/g.png')
dungeon_transfrom_flip('./output/b.png', './output/f.png')
dungeon_transfrom_flip('./output/c.png', './output/e.png')
dungeon_transfrom_flip('./output/ab.png', './output/fg.png')
dungeon_transfrom_flip('./output/bc.png', './output/ef.png')
dungeon_transfrom_flip('./output/cd.png', './output/de.png')
