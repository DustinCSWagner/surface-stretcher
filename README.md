# Description
This ruby script will transform a square image into the perspectives found in dungeon crawlers.

# Demonstration
![From my most recent game jam entry](example_animation.gif)


# Installation
Clone this repo.
```
> git clone https://gitlab.com/DustinCSWagner/surface-stretcher.git
> cd surface-stretcher
```

You will need to make sure you have ImageMagick installed on your system.
```
> magick -version
Version: ImageMagick 7.1.1-5 Q16-HDRI x86_64 20993 https://imagemagick.org
Copyright: (C) 1999 ImageMagick Studio LLC
License: https://imagemagick.org/script/license.php
Features: Cipher DPC HDRI Modules
Delegates (built-in): bzlib cairo djvu fftw fontconfig freetype heic jbig jng jp2 jpeg jxl lcms lqr ltdl lzma openexr pangocairo png raw rsvg tiff webp wmf x xml zlib
Compiler: gcc (13.0)
```

You will need to make sure you have ruby installed on your system.
```
> ruby -v
ruby 3.2.2 (2023-03-30 revision e51014f9c0) [x86_64-linux-gnu]
```

Install the image magick gem.
```
> gem install mini_magick
```

You are now ready to run the script.

# Running
Note that running stretch.rb will overwrite the existing images in the output directory.
Run the stretch.rb script with a file as the parameter:
```
>./stretch.rb './input/grass-brick-1280.png'
```

The transformed images will be in the outputs folder.
I usually then copy the output directory to my game's resource directory and give it an appropriate name.

# Using the images
I like to think of each image as a transparency slide, as there is a lot transparency in these images!
In my dungeon crawler games I check the nearby visible map (vm in the example below) and display the necessary images in order of furthest to closest from the point of view of a person walking through a dungeon. This way the closest images will obscure the furthest dungeon corridors as necessary.

My apologies if this is hard to read, but here is what I use for [DragonRuby](https://dragonruby.org/toolkit/game).
Note that the args.state.vm is a visible map array of 4 rows and 7 columns, the function decorate_wall returns the correct image based on the map code; '#' are walls 'm' are monsters, and render_pov is the function that reners the transparency stack of images.

```ruby
def decorate_wall(args, type, letter)
  case type
  when '#'
    return [args.state.view_x,
            args.state.view_y,
            args.state.view_width ,
            args.state.view_height,
            args.state.wall[letter]]
  when 'm'
    if letter.length == 1
      return [args.state.view_x,
              args.state.view_y,
              args.state.view_width ,
              args.state.view_height,
              args.state.monster[letter]]
    end
    # ...
  end
end

def render_pov(args)
  # furthest first, display background
  args.outputs.sprites << [args.state.view_x,
                           args.state.view_y,
                           args.state.view_width,
                           args.state.view_height,
                           args.state.bg]
  # A
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][0], 'a')
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][0], 'ab')

  # G
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][6], 'fg')
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][6], 'g')

  # B
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][1], 'b')
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][1], 'bc')

  # F
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][5], 'ef')
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][5], 'f')

  # C
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][2], 'c')
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][2], 'cd')

  # E
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][4], 'de')
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][4], 'e')

  # D
  args.outputs.sprites << decorate_wall(args, args.state.vm[0][3], 'd')

  #-------
  # Left of H
  args.outputs.sprites << decorate_wall(args, args.state.vm[1][0], 'h_ab')

  #right of L
  args.outputs.sprites << decorate_wall(args, args.state.vm[1][6], 'l_fg')

  # H
  args.outputs.sprites << decorate_wall(args, args.state.vm[1][1], 'h')
  args.outputs.sprites << decorate_wall(args, args.state.vm[1][1], 'hi_bc')

  # L
  args.outputs.sprites << decorate_wall(args, args.state.vm[1][5], 'l')
  args.outputs.sprites << decorate_wall(args, args.state.vm[1][5], 'kl_ef')

  # I
  args.outputs.sprites << decorate_wall(args, args.state.vm[1][2], 'i')
  args.outputs.sprites << decorate_wall(args, args.state.vm[1][2], 'ij_cd')

  # K
  args.outputs.sprites << decorate_wall(args, args.state.vm[1][4], 'k')
  args.outputs.sprites << decorate_wall(args, args.state.vm[1][4], 'jk_de')

  # J
  args.outputs.sprites << decorate_wall(args, args.state.vm[1][3], 'j')

  # Left of M
  args.outputs.sprites << decorate_wall(args, args.state.vm[2][0], 'm_hi')

  # right of O
  args.outputs.sprites << decorate_wall(args, args.state.vm[2][5], 'o_kl')

  # M-block
  args.outputs.sprites << decorate_wall(args, args.state.vm[2][2], 'mn_ij')
  args.outputs.sprites << decorate_wall(args, args.state.vm[2][2], 'm')

  # O-block
  args.outputs.sprites << decorate_wall(args, args.state.vm[2][4], 'o')
  args.outputs.sprites << decorate_wall(args, args.state.vm[2][4], 'no_jk')

  # N-block
  args.outputs.sprites << decorate_wall(args, args.state.vm[2][3], 'n')

  # P-block
  args.outputs.sprites << decorate_wall(args, args.state.vm[3][2], 'p')

  # Q-block
  args.outputs.sprites << decorate_wall(args, args.state.vm[3][4], 'q')

end
```

## Authors and acknowledgment
If this project does not work for you, try Zooperdan's [AtlasMaker for 2D Dungeon Crawlers](https://github.com/zooperdan/AtlasMaker-for-2D-Dungeon-Crawlers/)
Shoutout to the [dungeoncrawlers.org](https://www.dungeoncrawlers.org/), [DragonRuby](https://dragonruby.org), and [StableHorde](https://stablehorde.net/)!

## License
The code in stretch.rb is provided under the GPLv3 license.
All images in the inputs directory are under the CC0 license.

